# exercice 2- BeautifulSoup
import requests
from bs4 import BeautifulSoup
import re
UNI = requests.get("http://www.univ-orleans.fr")
soup = BeautifulSoup(UNI.text, "lxml")
print(soup.h1)
# recupère toutes les balises <div>
print(soup.find_all('div'))
# recupère toutes les img
print(soup.find_all('img'))
# recupère tous les liens
print(soup.find_all('a'))

print(soup.find_all('div', re.compile(r'composite-zone')))
