import requests
from bs4 import BeautifulSoup


def get_definition(x):
    """
    get word definition from aonaware site
    """
    URL = 'http://services.aonaware.com/DictService/Default.aspx?action=define&dict=wn&query={0}'.format(
        x)
    html = requests.get(URL).text


def get_def_requests(x):
    payload = {'action': 'define', 'dict': '*', 'query': x}
    UNI = requests.get(
        'http://services.aonaware.com/DictService/Default.aspx?', params=payload)
    soup = BeautifulSoup(UNI.text, 'lxml')
    print(soup.select('#lblDefinition'))


lines = []
with open('vocabulary.txt') as f:
    lines = f.readlines()
    for m in lines:
        get_def_requests(m)
