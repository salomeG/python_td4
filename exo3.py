# exercice 3- Stackoverflow
import re
import requests
from bs4 import BeautifulSoup
#import pandas
UNI = requests.get("https://stackoverflow.com/questions/tagged/beautifulsoup")
soup = BeautifulSoup(UNI.text, "lxml")
print(soup.find_all('a', class_='question-hyperlink', limit=10))
print("")
print(soup.select('.question-hyperlink', limit=10))
print(soup.select('.votes', limit=10))
print(soup.select('.status unanswered', limit=10))
